package main;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.createnwes.matatabifutatabi.rest.dto.ProgramTimesSpotDto;


public class SampleXml {
	
	public static void main(String[] args){
		
		SampleXml thread = new SampleXml();
		thread.execute();
		
	}
	
	private void execute(){
		
		try {
			
			String urlParam = "http://maps.google.com/maps/api/geocode/xml?latlng=35.744449,139.715035&sensor=false&language=ja";
			
			URL url = new URL(urlParam);
			
			URLConnection uc = url.openConnection();
			
			InputStream is = uc.getInputStream();
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder document = factory.newDocumentBuilder();
			
			Document root = document.parse(is);
			
			Node node = root.getFirstChild();
			
			NodeList nList = node.getChildNodes();
			
			for(int i = 0; i < nList.getLength(); i++){
				
				Node node2 = nList.item(i);
				
				if(node2 != null && node2.getNodeName().equals("result")){
					
					NodeList nList2 = node2.getChildNodes();
					addressComponent(nList2);
					
				}
				
			}
			
			is.close();
			
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		

		
	}
	
	private void addressComponent(NodeList list){
		
		for(int i = 0; i < list.getLength(); i++){
			
			Node node = list.item(i);
			
			if(node.getNodeName().equals("address_component")){
				types(node);
			}
			
		}
		
	}
	
	private void types(Node node){
		
		String longName = "";
		List<String> type = new ArrayList<String>();
		
		NodeList nList = node.getChildNodes();
		
		for(int i = 0; i < nList.getLength(); i++){
			
			Node node2 = nList.item(i);
			
//			System.out.println(node2.getNodeName());
//			System.out.println(node2.getTextContent());
			
			if("long_name".equals(node2.getNodeName())){
				longName = node2.getTextContent();
			}else if("type".equals(node2.getNodeName())){
				type.add(node2.getTextContent());
			}
			
		}
		
		try{
			Thread.sleep(0);
		}catch(Exception e){
			
		}
		
		for(String str : type){
			
//			System.out.println(str);
			
			if("country".equals(str)){
				System.out.println("country:" + longName);
				break;
			}else if("administrative_area_level_1".equals(str)){
				if(longName.endsWith("都") || longName.endsWith("府") || longName.endsWith("県")){
					longName = longName.substring(0, longName.length() -1);
				}
				
				System.out.println("administrative_area_level_1:" + longName);
				break;
				
			}else if("locality".equals(str)){
				System.out.println("locality:" + longName);
				break;
			}
			
		}
		
	}

}
