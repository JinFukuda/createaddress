package main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.createnwes.matatabifutatabi.rest.dto.ProgramTimesSpotDto;
import com.createnwes.matatabifutatabi.rest.exception.ApplicationException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import dao.SelectProgramTimesSpot;
import dao.UpdateProgramTimesSpot;


public class ParserObject {
	
	private List<ProgramTimesSpotDto> dtos;

	public void execute() throws ApplicationException, SQLException, IOException{
		
		SelectProgramTimesSpot selectProgramTimesSpot = new SelectProgramTimesSpot();
		
		selectProgramTimesSpot.execute();
		
		dtos = selectProgramTimesSpot.getDtos();
		
		for(ProgramTimesSpotDto dto : dtos){
			
			if(dto.getLatitude() == 0 && dto.getLongitude() == 0){
				continue;
			}
			
			String urlParam = "http://maps.google.com/maps/api/geocode/json?latlng=" + dto.getLatitude() + "," + dto.getLongitude() + "&sensor=false&language=ja";
			
			URL url = new URL(urlParam);
			
			URLConnection uc = url.openConnection();
			
			InputStream is = uc.getInputStream();
			
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			
			String json = "";
			String line = "";
			
			while((line = br.readLine()) != null){
				json += line;
			}
			
			br.close();
			
			JsonFactory factory = new JsonFactory();
			
			JsonParser parser = factory.createJsonParser(json);
			
			while(parser.nextToken() != JsonToken.END_OBJECT) {
				String name = parser.getCurrentName();
				if(name != null){
					if(name.equals("results")){
						addressComponents(parser, dto);
						break;
					}
				}
			}
			
			UpdateProgramTimesSpot updateProgramTimesSpot = new UpdateProgramTimesSpot(dto);
			updateProgramTimesSpot.execute();
		}
		
	}
	
	private void addressComponents(JsonParser parser, ProgramTimesSpotDto dto) throws JsonParseException, IOException{
		
		String addres1 = "";
		String pref = "";
		String coountry = "";
		
		
		while(parser.nextToken() != JsonToken.END_ARRAY){
			
			String name = parser.getCurrentName();
			parser.nextToken();
			if(name != null && name.equals("address_components")){
				
				parser.nextToken();
				
				int i = 0;
				
				while(parser.nextToken() != JsonToken.END_ARRAY){
				
					parser.nextToken();
					
					types(parser, dto);
					
					i++;
					
				}
				
				break;
				
			}
		}
		
	}
	
	private static void types(JsonParser parser, ProgramTimesSpotDto dto) throws JsonParseException, IOException{
		
		String long_name = "";
		List<String> types = new ArrayList<String>();
		
		while(parser.nextToken() != JsonToken.END_ARRAY){
			
			parser.nextToken();
			
			String name = parser.getCurrentName();
			String value = parser.getText();
			
			System.out.println(name + ":" + value);
			
			if((name == null || "null".equals(name.trim())) && (value == null || "null".equals(value.trim()))){
				break;
			}
			
			
			if(name != null && "long_name".equals(name)){
				long_name = parser.getText();
			}else if(name != null && "types".equals(name)){
				while(parser.nextToken() != JsonToken.END_ARRAY){
					parser.nextToken();
					String name2 = parser.getCurrentName();
					String value2 = parser.getText();
					
					if("types".equals(name2)){
						break;
					}
					
					types.add(value2);
				}
			}
		}
		
		for(String str : types){
			
			if("locality".equals(str)){
				dto.setAddress1(long_name);
			}else if("administrative_area_level_1".equals(str)){
				dto.setPref(long_name);
			}else if("country".equals(str)){
				dto.setCountry(long_name);
			}
			
		}
	}
	
}
