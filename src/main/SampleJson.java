package main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;


public class SampleJson {
	
	public static void main(String[] args) throws IOException{
		
		String url = "http://maps.google.com/maps/api/geocode/json?latlng=35.777497,139.649556&sensor=false&language=ja";
		
		URL changeURL = new URL(url);
		
		URLConnection uc = changeURL.openConnection();
		
		InputStream is = uc.getInputStream();
		
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		
		String json = "";
		String line = "";
		
		while((line = br.readLine()) != null){
			System.out.println(line);
			json += line;
		}
		br.close();
		
		JsonFactory factory = new JsonFactory();
		
		JsonParser parser = factory.createJsonParser(json);
		
		while (parser.nextToken() != JsonToken.END_OBJECT) {
			String name = parser.getCurrentName();
			if(name != null){
				if(name.equals("results")){
					addressComponents(parser);
					break;
				}
			}
		}
		
	}
	
	private static void addressComponents(JsonParser parser) throws JsonParseException, IOException{
		
		while(parser.nextToken() != JsonToken.END_ARRAY){
			
			String name = parser.getCurrentName();
			parser.nextToken();
			if(name != null && name.equals("address_components")){
				
				parser.nextToken();
				
				int i = 0;
				
				while(parser.nextToken() != JsonToken.END_ARRAY){
				
					parser.nextToken();
					
					types(parser);
						
					i++;
					
				}
				
				break;
				
			}
		}
		
	}
	
	private static String types(JsonParser parser) throws JsonParseException, IOException{
		
		String long_name = "";
		List<String> types = new ArrayList<String>();
		
		while(parser.nextToken() != JsonToken.END_ARRAY){
			
			parser.nextToken();
			
			String name = parser.getCurrentName();
			String value = parser.getText();
			
			if((name == null || "null".equals(name)) && (value == null || "null".equals(value))){
				break;
			}
			
			
			if(name != null && "long_name".equals(name)){
				long_name = parser.getText();
				System.out.println(name + ":" + value);
			}else if(name != null && "types".equals(name)){
				while(parser.nextToken() != JsonToken.END_ARRAY){
					parser.nextToken();
					String name2 = parser.getCurrentName();
					String value2 = parser.getText();
					
					System.out.println(name2 + ":" + value2);
					
					if("types".equals(name2)){
						break;
					}
					
					types.add(value2);
				}
			}
			
			/*
			if(name != null){
				if("long_name".equals(name) || "short_name".equals(name) || "types".equals(name)){
					System.out.println(name + ":" + value);
				}
			}
			*/
		}
		
		return "";
	}

}
