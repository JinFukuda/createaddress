package main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.createnwes.matatabifutatabi.rest.dto.ProgramTimesSpotDto;
import com.createnwes.matatabifutatabi.rest.exception.ApplicationException;

import dao.SelectProgramTimesSpot;
import dao.UpdateProgramTimesSpot;


public class ParserXmlObject {
	
	private List<ProgramTimesSpotDto> dtos;
	
	public void execute() throws ApplicationException, SQLException, IOException, ParserConfigurationException, SAXException{
		
		SelectProgramTimesSpot selectProgramTimesSpot = new SelectProgramTimesSpot();
		
		selectProgramTimesSpot.execute();
		
		dtos = selectProgramTimesSpot.getDtos();
		
		for(ProgramTimesSpotDto dto : dtos){
			
			if(dto.getCountry() != null && !"".equals(dto.getCountry()) ){
				continue;
			}
			
			String urlParam = "http://maps.google.com/maps/api/geocode/xml?latlng=" + dto.getLatitude() + "," + dto.getLongitude() + "&sensor=false&language=ja";
			//String urlParam = "http://maps.google.com/maps/api/geocode/xml?latlng=35.744449,139.715035&sensor=false&language=ja";
			
			URL url = new URL(urlParam);
			
			URLConnection uc = url.openConnection();
			
			InputStream is = uc.getInputStream();
			
			
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder document = factory.newDocumentBuilder();
				
				Document root = document.parse(is);
				
				Node node = root.getFirstChild();
				
				NodeList nList = node.getChildNodes();
				
				for(int i = 0; i < nList.getLength(); i++){
					
					Node node2 = nList.item(i);
					
					if(node2 != null && node2.getNodeName().equals("result")){
						
						NodeList nList2 = node2.getChildNodes();
						addressComponent(nList2, dto);
						
					}
					
				}
				
			}catch( ParserConfigurationException e ) {
				e.printStackTrace();
			}catch( SAXException e ) {
				e.printStackTrace();
			}catch( IOException e ) {
				e.printStackTrace();
			}
			
			if(dto.getProgramTimesSpotId() == 41){
				try{
					Thread.sleep(0);
				}catch(Exception e){
					
				}
			}
			
			is.close();
			
			UpdateProgramTimesSpot updateProgramTimesSpot = new UpdateProgramTimesSpot(dto);
			updateProgramTimesSpot.execute();
			
		}
		
	}
	
	private void addressComponent(NodeList list, ProgramTimesSpotDto dto){
		
		for(int i = 0; i < list.getLength(); i++){
			
			Node node = list.item(i);
			
			if(node.getNodeName().equals("address_component")){
				types(node, dto);
			}
			
		}
		
	}
	
	private void types(Node node, ProgramTimesSpotDto dto){
		
		if(dto.getProgramTimesSpotId() == 41){
			try{
				Thread.sleep(0);
			}catch(Exception e){
				
			}
		}
		
		String longName = "";
		List<String> type = new ArrayList<String>();
		
		NodeList nList = node.getChildNodes();
		
		for(int i = 0; i < nList.getLength(); i++){
			
			Node node2 = nList.item(i);
			
			System.out.println(node2.getNodeName());
			System.out.println(node2.getTextContent());
			
			if("long_name".equals(node2.getNodeName())){
				longName = node2.getTextContent();
			}else if("type".equals(node2.getNodeName())){
				type.add(node2.getTextContent());
			}
			
		}
		
		for(String str : type){
			
			if("country".equals(str)){
				dto.setCountry(longName);
				break;
			}else if("administrative_area_level_1".equals(str)){
				if(longName.endsWith("都") || longName.endsWith("府") || longName.endsWith("県")){
					longName = longName.substring(0, longName.length() -1);
				}
				
				dto.setPref(longName);
				
			}else if("locality".equals(str)){
				dto.setAddress1(longName);
			}
			
		}
		
	}

}
