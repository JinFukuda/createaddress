package dao;

import java.sql.SQLException;

import com.createnwes.matatabifutatabi.rest.dao.AbstractDao;
import com.createnwes.matatabifutatabi.rest.dto.ProgramTimesSpotDto;
import com.createnwes.matatabifutatabi.rest.exception.ApplicationException;

public class UpdateProgramTimesSpot extends AbstractDao {

	private ProgramTimesSpotDto dto;
	
	public UpdateProgramTimesSpot(ProgramTimesSpotDto dto) throws ApplicationException {
		super();
		this.dto = dto;
	}

	@Override
	public void execute() throws SQLException {
		
		
		
		String sql = "";
		
		sql += " update program_times_spot set ";
		sql += "     country = ?, ";
		sql += "     pref = ?, ";
		sql += "     address1 = ? ";
		sql += " where ";
		sql += "     program_times_spot_id = ? ";
		
		stmt = conn.prepareStatement(sql);
		stmt.setString(1, dto.getCountry());
		stmt.setString(2, dto.getPref());
		stmt.setString(3, dto.getAddress1());
		stmt.setInt(4, dto.getProgramTimesSpotId());
		
		stmt.executeUpdate();
		
//		conn.commit();
		
	}

}
