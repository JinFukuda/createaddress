package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.createnwes.matatabifutatabi.rest.dao.AbstractDao;
import com.createnwes.matatabifutatabi.rest.dto.ProgramTimesSpotDto;
import com.createnwes.matatabifutatabi.rest.exception.ApplicationException;

public class SelectProgramTimesSpot extends AbstractDao {

	private List<ProgramTimesSpotDto> dtos;

	public SelectProgramTimesSpot() throws ApplicationException {
		super();
	}

	@Override
	public void execute() throws SQLException {

		String sql = "";

		sql += " select ";
		sql += "     program_times_spot_id, ";
		sql += "     program_name_id, ";
		sql += "     program_times_id, ";
		sql += "     latitude, ";
		sql += "     longitude, ";
		sql += "     spot_explain, ";
		sql += "     country, ";
		sql += "     pref, ";
		sql += "     address1, ";
		sql += "     delete_flg, ";
		sql += "     version ";
		sql += " from  ";
		sql += "     program_times_spot ";
		
		stmt = conn.prepareStatement(sql);
		
		rs = stmt.executeQuery();
		
		dtos = new ArrayList<ProgramTimesSpotDto>();
		ProgramTimesSpotDto dto;
		
		while(rs.next()){
			
			dto = new ProgramTimesSpotDto();
			
			dto.setProgramTimesSpotId(rs.getInt("program_times_spot_id"));
			dto.setProgramNameId(rs.getInt("program_name_id"));
			dto.setProgramTimesId(rs.getInt("program_times_id"));
			dto.setLatitude(rs.getDouble("latitude"));
			dto.setLongitude(rs.getDouble("longitude"));
			dto.setSpotExplain(rs.getString("spot_explain"));
			dto.setCountry(rs.getString("country"));
			dto.setPref(rs.getString("pref"));
			dto.setAddress1(rs.getString("address1"));
			dto.setDeleteFlag(rs.getInt("delete_flg"));
			dto.setVersion(rs.getInt("version"));
			
			dtos.add(dto);
		}
		
	}

	/**
	 * dtosを取得します。
	 * @return dtos
	 */
	public List<ProgramTimesSpotDto> getDtos() {
	    return dtos;
	}

	/**
	 * dtosを設定します。
	 * @param dtos dtos
	 */
	public void setDtos(List<ProgramTimesSpotDto> dtos) {
	    this.dtos = dtos;
	}
	
	
}
